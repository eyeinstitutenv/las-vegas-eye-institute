Matthew Swanic M.D. is a UCLA fellowship-trained cornea and refractive surgeon who specializes in advanced LASIK and cataract surgery. He is certified on the advanced Visx iDesign 2.0 platform and performs bladeless LASIK using the Carl Zeiss Visumax femtosecond laser at the Las Vegas Eye Institute.


Address: 9555 S Eastern Ave, Suite 260, Las Vegas, NV 89123, USA

Phone: 702-816-2525

Website: https://lasvegaseyeinstitute.com
